<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Masterpiece Jewelry Studio</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">

    <!-- FontAwesome CSS for nice icons -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="css/custom.min.css" rel="stylesheet">

</head>

<body>
<h1 class="site-heading text-center text-white d-none d-lg-block">
    <span class="site-heading-upper text-primary mb-3">Masterpiece Jewelry Studio</span>
    <span class="site-heading-lower"></span>
</h1>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark py-lg-4  bg-primary" id="mainNav">
    <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="index.html">
            Masterpiece Jewelry Studio
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="index.html">
                        Home
                    </a>
                </li>
                <li class="nav-item active px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="products.php">
                        Products
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="services.html">
                        Services
                    </a>
                </li>
                <li class="nav-item px-lg-4">
                    <a class="nav-link text-uppercase text-expanded" href="testimonials.html">
                        Testimonials
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Sort Dropdown -->
<div class="container">
    <div class="row justify-content-end">
        <h5 class="mt-3 mr-1">Sort By</h5>
        <div class="dropdown m-1 mr-3"
             data-jplist-control="dropdown-sort"
             data-opened-class="show"
             data-group="product-group"
             data-name="sort1">
            <button data-type="panel"
                    class="btn btn-primary dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton">
                Featured
            </button>
            <div data-type="content"
                 class="dropdown-menu"
                 aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item"
                   href="#"
                   data-path=".featured"
                   data-order="desc"
                   data-type="number">
                    Featured
                </a>
                <a class="dropdown-item"
                   href="#"
                   data-path=".price"
                   data-order="asc"
                   data-type="number">
                    Price: Low to High
                </a>
                <a class="dropdown-item"
                   href="#"
                   data-path=".price"
                   data-order="desc"
                   data-type="number">
                    Price: High to Low
                </a>
                <a class="dropdown-item"
                   href="#"
                   data-path=".date-added"
                   data-order="desc"
                   data-type="datetime"
                   data-datetime-format="{year}/{month}/{day}">
                    Newest Arrivals
                </a>
            </div>
        </div>
    </div>
</div>


<!-- Page Content -->
<div class="container">
    <div class="row">

        <!-- Product Filters -->
        <div class="col-lg-3">
            <div class="list-group">
                <a href="#collapseJewelryType"
                   class="list-group-item"
                   data-toggle="collapse"
                   role="button"
                   aria-expanded="true"
                   aria-controls="collapseJewelryType">
                    Jewelry Type
                </a>
                <div class="collapse show" id="collapseJewelryType">
                    <div class="card card-body">
                        <label class="m-0">
                            <input type="checkbox"
                                   data-jplist-control="checkbox-text-filter"
                                   data-group="product-group"
                                   data-or="bag"
                                   data-path=".type"
                                   value="bracelet">
                            Bracelet
                        </label>

                        <label class="m-0">
                            <input type="checkbox"
                                   data-jplist-control="checkbox-text-filter"
                                   data-group="product-group"
                                   data-or="bag"
                                   data-path=".type"
                                   value="ear">
                            Earring
                        </label>

                        <label class="m-0">
                            <input type="checkbox"
                                   data-jplist-control="checkbox-text-filter"
                                   data-group="product-group"
                                   data-or="bag"
                                   data-path=".type"
                                   value="necklace">
                            Necklace
                        </label>

                        <label class="m-0">
                            <input type="checkbox"
                                   data-jplist-control="checkbox-text-filter"
                                   data-group="product-group"
                                   data-or="bag"
                                   data-path=".type"
                                   value="ring">
                            Ring
                        </label>

                        <label class="m-0">
                            <input type="checkbox"
                                   data-jplist-control="checkbox-text-filter"
                                   data-group="product-group"
                                   data-or="bag"
                                   data-path=".type"
                                   value="watch">
                            Watch
                        </label>
                    </div>
                </div>

                <div class="list-group">
                    <a href="#collapsePriceForm"
                       class="list-group-item"
                       data-toggle="collapse"
                       role="button"
                       aria-expanded="true"
                       aria-controls="collapsePriceForm">
                        Price
                    </a>
                    <div class="collapse show" id="collapsePriceForm">
                        <div class="card card-body">
                            <div data-jplist-control="slider-range-filter"
                                 data-path=".price"
                                 data-group="product-group"
                                 data-name="price-filter"
                                 data-min="0"
                                 data-from="0"
                                 data-to="3000"
                                 data-max="3000">

                                <div class="custom-range" data-type="slider"></div>

                                <div class="jplist-slider-values">
                                    <b>Low:</b> <span data-type="value-1"></span>,
                                    <b>High:</b> <span data-type="value-2"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="list-group">
                    <a href="#collapseMetals"
                       class="list-group-item"
                       data-toggle="collapse"
                       role="button"
                       aria-expanded="false"
                       aria-controls="collapseMetals">
                        Metals
                    </a>
                    <div class="collapse" id="collapseMetals">
                        <div class="card card-body">
                            <?php
                            $unique_metals = array();

                            # Collect the unique tags
                            foreach (glob("products/*") as $file) {
                                $meta = json_decode(file_get_contents("$file"), true);
                                foreach ($meta["metals"] as $metal) {
                                    $metal = strtolower($metal);
                                    if (! in_array($metal, $unique_metals)) {
                                        array_push($unique_metals, $metal);
                                    }
                                }
                            }

                            # Sort the the tags alphabetically
                            asort($unique_metals);

                            # Create the filter check boxes and labels
                            $metal_filter_format = '
                                <label class="m-0">
                                    <input type="checkbox"
                                           data-jplist-control="checkbox-text-filter"
                                           data-group="product-group"
                                           data-path=".metals"
                                           data-or="bag"
                                           value="%s">
                                    %s
                                </label>
                            ';

                            foreach ($unique_metals as $metal) {
                                echo sprintf(
                                    $metal_filter_format,
                                    $metal,
                                    ucwords($metal)
                                );
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="list-group">
                    <a href="#collapseJewelTypes"
                       class="list-group-item"
                       data-toggle="collapse"
                       role="button"
                       aria-expanded="false"
                       aria-controls="collapseJewelTypes">
                        Jewel Type
                    </a>
                    <div class="collapse" id="collapseJewelTypes">
                        <div class="card card-body">
                            <?php
                            $unique_jewel_types = array();

                            # Collect the unique tags
                            foreach (glob("products/*") as $file) {
                                $meta = json_decode(file_get_contents("$file"), true);
                                foreach ($meta["jewel_types"] as $jewel_type) {
                                    $jewel_type = strtolower($jewel_type);
                                    if (! in_array($jewel_type, $unique_jewel_types)) {
                                        array_push($unique_jewel_types, $jewel_type);
                                    }
                                }
                            }

                            # Sort the the tags alphabetically
                            asort($unique_jewel_types);

                            # Create the filter check boxes and labels
                            $jewel_type_filter_format = '
                                <label class="m-0">
                                    <input type="checkbox"
                                           data-jplist-control="checkbox-text-filter"
                                           data-group="product-group"
                                           data-path=".jewel_types"
                                           data-or="bag"
                                           value="%s">
                                    %s
                                </label>
                            ';

                            foreach ($unique_jewel_types as $jewel_type) {
                                echo sprintf(
                                    $jewel_type_filter_format,
                                    $jewel_type,
                                    ucwords($jewel_type)
                                );
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="list-group">
                    <a href="#collapseTags"
                       class="list-group-item"
                       data-toggle="collapse"
                       role="button"
                       aria-expanded="false"
                       aria-controls="collapseTags">
                        Tags
                    </a>
                    <div class="collapse" id="collapseTags">
                        <div class="card card-body">
                            <?php
                            $unique_tags = array();

                            # Collect the unique tags
                            foreach (glob("products/*") as $file) {
                                $meta = json_decode(file_get_contents("$file"), true);
                                foreach ($meta["tags"] as $tag) {
                                    $tag = strtolower($tag);
                                    if (! in_array($tag, $unique_tags)) {
                                        array_push($unique_tags, $tag);
                                    }
                                }
                            }

                            # Sort the the tags alphabetically
                            asort($unique_tags);

                            # Create the filter check boxes and labels
                            $tag_filter_format = '
                                <label class="m-0">
                                    <input type="checkbox"
                                           data-jplist-control="checkbox-text-filter"
                                           data-group="product-group"
                                           data-path=".tags"
                                           value="%s">
                                    %s
                                </label>
                            ';

                            foreach ($unique_tags as $tag) {
                                echo sprintf($tag_filter_format, $tag, ucwords($tag));
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Products -->
        <div class="col-lg-9">
            <div class="row" data-jplist-group="product-group">

                <?php
                foreach (glob("products/*") as $file) {
                    $meta = json_decode(file_get_contents("$file"), true);
                    switch ($p = $meta["price"]) {
                        case gettype($p) == "string":
                            $price = $p;
                            break;
                        case (gettype($p) == "integer") or (gettype($p) == "double"):
                            $price = sprintf("$%0.2f", $p);
                            break;
                        default:
                            echo gettype($p) . $p;
                    }

                    $tags = "";
                    $tags_str = "";
                    $tag_format = '<span class="badge badge-primary">%s</span>';
                    foreach ($meta["tags"] as $tag) {
                        $tags = $tags . "\n" . sprintf($tag_format, ucwords($tag));
                        $tags_str = $tags_str . " " . strtolower($tag);
                    }

                    $metals_str = "";
                    foreach ($meta["metals"] as $metal) {
                        $metals_str = $metals_str . " " . strtolower($metal);
                    }

                    $jewel_types_str = "";
                    foreach ($meta["jewel_types"] as $jewel_types) {
                        $jewel_types_str = $jewel_types_str . " " . strtolower($jewel_types);
                    }

                    $tiny_img = str_replace(".jpg", "_tiny.jpg", $meta["img"]);

                    $format = '
                            <div data-jplist-item class="col-lg-4 col-md-6 mb-4 product">
                                <div class="featured" style="display:none">%d</div>
                                <div class="price" style="display:none">%0.2f</div>
                                <div class="date-added" style="display:none">%s</div>
                                <div class="type" style="display:none">%s</div>
                                <div class="tags" style="display:none">%s</div>
                                <div class="jewel_types" style="display:none">%s</div>
                                <div class="metals" style="display:none">%s</div>
                                <div class="card h-100">
                                    <a href="%s" class="progressive replace card-img-top">
                                        <img class="preview" src="%s" alt="">
                                    </a>
                                    <div class="card-body">
                                        <h5>%s</h5>
                                        <p class="card-text">%s</p>
                                        %s
                                    </div>
                                </div>
                            </div>
                        ';
                    echo sprintf(
                        $format,
                        $meta["featured"],
                        $meta["price"],
                        $meta["date-added"],
                        $meta["type"],
                        $tags_str,
                        $jewel_types_str,
                        $metals_str,
                        $meta["img"],
                        $tiny_img,
                        $price,
                        $meta["description"],
                        $tags
                    );
                }
                ?>

                <!-- no results control -->
                <div data-jplist-control="no-results"
                     data-group="product-group"
                     data-name="no-results"
                     class="col text-xl-center">
                    No Results Found
                </div>
            </div>
        </div>

    </div>
</div>

<button id="back-to-top"
        type="button"
        class="btn btn-lg btn-primary back-to-top">
    <span class="fa fa-chevron-up fa-2x" aria-hidden="true"></span>
</button>

<footer class="footer text-white text-center py-5 bg-primary">
    <div class="container">
        <p class="m-0 small">
            2557 County Route 5
            <br>
            New Lebanon, NY 12125-3111
            <br>
            (518) 794-0007
            <br>
            Copyright &copy; Masterpiece Studio 2020
        </p>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jPList Library -->
<script src="jplist/jplist.min.js"></script>
<script>jplist.init();</script>
<!-- Jump to top button -->
<script src="scroll_to_top.js"></script>
<!-- Smooth scrolling page buttons-->
<script src="smooth_scroll.js"></script>
<!-- Resources for progressive loading of images -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/progressive-image.js/dist/progressive-image.css">
<script src="https://cdn.jsdelivr.net/npm/progressive-image.js/dist/progressive-image.js"></script>
</body>
</html>
